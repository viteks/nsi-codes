/**
* @author Marcus Lee
*
* @section DESCRIPTION
*    A library for the AMS ENS210 tempuratire and relative humidy sensor.
*
*/ 


#ifndef AMS_ENS210_H
#define AMS_ENS210_H

#include "mbed.h"


/* Library defaults */
#define CONFIG_TEMP_OP_MODE     0                               // single shot
#define CONFIG_HUMID_OP_MODE    0                               // single shot
#define CONFIG_POWER_MODE       1                               // low power

/* Library Constants */
#define ENS210_SLAVE_ADDR_RAW   0x43
#define ENS210_SLAVE_ADDR       ENS210_SLAVE_ADDR_RAW << 1     // 0x86
#define ENS210_SLAVE_ADDR_W     ENS210_SLAVE_ADDR
#define ENS210_SLAVE_ADDR_R     ENS210_SLAVE_ADDR | 1          // 0x87

#define SYS_CONFIG              0x10
#define SYS_STATUS              0x11

#define SENS_OP_MODE            0x21
#define SENS_START              0x22
#define SENS_STOP               0x23
#define SENS_STATUS             0x24
#define SENS_TEMP               0x30
#define SENS_HUMID              0x33

/** The AMS ENS210 class
 */
class AMS_ENS210 {
    
    public:
        /** Create an AMS_ENS210 instance
         * 
         * @param i2c   The I2C interface to use for communication
         */
        AMS_ENS210(I2C * i2c);
        
        /** Create an AMS_ENS210 instance
         * 
         * @param i2c                   The I2C interface to use for communication
         * @param temp_continuous       Set tempurature operation mode, true for continuous and false for single shot
         * @param humid_continuous      Set humidity operation mode, true for continuous and false for single shot
         */
        AMS_ENS210(I2C * i2c, bool temp_continuous, bool humid_continuous);
        
        /** Create an AMS_ENS210 instance
         * 
         * @param i2c                   The I2C interface to use for communication
         * @param temp_continuous       Set tempurature operation mode, true for continuous and false for single shot
         * @param humid_continuous      Set humidity operation mode, true for continuous and false for single shot
         * @param low_power             Set power mode, true for low power/standby and false for active
         */
        AMS_ENS210(I2C * i2c, bool temp_continuous, bool humid_continuous, bool low_power);
        
        /** Destroy the AMS_ENS210 instance
         */
        ~AMS_ENS210();
        
        /** Initalise the sensor
         *
         * @return Intalisation success
         */
        bool init();
        
        /** Software reset the sensor
         *
         * @return Reset success
         */
        bool reset();
        
        /** Set the power mode 
         * 
         * @param low_power     True for low power/standby and false for active
         *
         * @return Write success
         */
        bool low_power_mode(bool low_power);
        
        /** Get the current power mode
         *
         * @return The power mode, true for low power, false for active
         */
        bool low_power_mode();
        
        /** Get whether the sensor is in the active state or not
         *
         * @return The active state, true for active, false for inactive
         */
        bool is_active();
        
        /** Set the tempurature operation mode 
         * 
         * @param single_shot      True for continuous and false for single shot
         *
         * @return Write success
         */
        bool temp_continuous_mode(bool continuous);
        
        /** Get the current tempurature operation mode
         *
         * @return Write success
         */
        bool temp_continuous_mode();
        
        /** Set the humidity operation mode 
         * 
         * @param single_shot      True for continuous and false for single shot
         *
         * @return Write success
         */
        bool humid_continuous_mode(bool continuous);
        
        /** Get the current humidity operation mode
         *
         * @return Write success
         */
        bool humid_continuous_mode();
        
        /** Set the I2C interface
         * 
         * @param i2c   The I2C interface
         */
        void i2c_interface(I2C * i2c);
        
        /** Get the I2C interface
         * 
         * @return The I2C interface
         */
        I2C* i2c_interface();
        
        /** Start measurement collection
         *
         * @param temp      Start the tempurature sensor
         * @param humid     Start the humidity sensor
         *
         * @return Write success
         */
        bool start(bool temp = true, bool humid = true);
        
        /** Stop measurement collection
         *
         * @param temp      Stop the tempurature sensor
         * @param humid     Stop the humidity sensor
         *
         * @return Write success
         */
        bool stop(bool temp = true, bool humid = true);
        
        /** Get whether the sensor is measuring tempurature data
         *
         * @return Sensor activity, true for measureing, false for idle
         */
        bool temp_is_measuring();
        
        /** Get whether the sensor is measuring humidity data
         *
         * @return Sensor activity, true for measureing, false for idle
         */
        bool humid_is_measuring();
        
        /** Get whether the sensor has collected tempurature data
         *
         * @return Has data
         */
        bool temp_has_data();
        
        /** Get whether the sensor has collected humidity data
         *
         * @return Has data
         */
        bool humid_has_data();
        
        /** Get the most recent tempurature measurement, must call temp_has_data() first when in single shot mode otherwise the same data will be returned 
         *
         * @return Most recent tempurature measurement as a 16 bits unsigned value in 1/64 Kelvin
         */
        uint16_t temp_read();
        
        /** Get the most recent humidty measurement, must call humid_has_data() first when in single shot mode otherwise the same data will be returned 
         *
         * @return Most recent humidity measurement as a 16 bits unsigned value in 1/64 Kelvin
         */
        uint16_t humid_read();
        
    private:
        I2C * _i2c;
        bool _temp_mode;
        bool _humid_mode;
        bool _power_mode;
        bool _reset;
        uint16_t temp_reading;
        uint16_t humid_reading;
        
        bool write_config(bool system = true, bool sensor =  true);
        const char * read_config(bool system = true, bool sensor =  true);
        int i2c_read(char reg_addr, char* output, int len);
        int i2c_write(char reg_addr, char* input, int len);
        
        
        
};

#endif /* AMS_ENS210_H */