/***************************************************************************//**
 * @file Si7210.h
 * @brief Driver class for the Silicon Labs Si7210 I2C Hall-effect sensor
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2017 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/
#ifndef SI7210_H
#define SI7210_H

#include "mbed.h"

/* Possible I2C slave addresses */
#define SI7210_ADDRESS_0    (0x30U << 1)
#define SI7210_ADDRESS_1    (0x31U << 1)
#define SI7210_ADDRESS_2    (0x32U << 1)
#define SI7210_ADDRESS_3    (0x33U << 1)

/* Register addresses */
#define SI72XX_HREVID       0xC0U
#define SI72XX_DSPSIGM      0xC1U
#define SI72XX_DSPSIGL      0xC2U
#define SI72XX_DSPSIGSEL    0xC3U
#define SI72XX_POWER_CTRL   0xC4U
#define SI72XX_ARAUTOINC    0xC5U
#define SI72XX_CTRL1        0xC6U
#define SI72XX_CTRL2        0xC7U
#define SI72XX_SLTIME       0xC8U
#define SI72XX_CTRL3        0xC9U
#define SI72XX_A0           0xCAU
#define SI72XX_A1           0xCBU
#define SI72XX_A2           0xCCU
#define SI72XX_CTRL4        0xCDU
#define SI72XX_A3           0xCEU
#define SI72XX_A4           0xCFU
#define SI72XX_A5           0xD0U
#define SI72XX_OTP_ADDR     0xE1U
#define SI72XX_OTP_DATA     0xE2U
#define SI72XX_OTP_CTRL     0xE3U
#define SI72XX_TM_FG        0xE4U

#define SI72XX_ERROR_BUSY   0xFEU
#define SI72XX_ERROR_NODATA 0xFDU

/* Possible (bipolar) measurement range settings */
typedef enum {
    RANGE_20mT,
    RANGE_200mT
} Si7210_range_t;

namespace silabs {
class Si7210
{
public:
    Si7210(PinName sda, PinName scl, uint8_t address);
    Si7210(I2C *i2c_bus, uint8_t address = SI7210_ADDRESS_0);
    ~Si7210();
    
    /*
     * Get last measured temperature data
     * return: int = temperature in 1/4th degrees centigrade
     */
    int getTemperature();

    /*
     * Get last measured field strength
     * return: int = field strength in micro-Tesla.
     */
    int getFieldStrength();
    
    /*
     * Set measurement range.
     * Return true if successful, false if device is not responding.
     */
    bool setFieldStrength(Si7210_range_t range);
    
    /*
     * Return true if successful, false if device is not responding.
     */
    bool fetchFieldStrength();
    
    bool measureOnce();
    
    /*
     * Check if the sensor is active and responding.
     */
    bool check();
    
    /* Read a register from Si7210 */
    bool readRegister(uint8_t reg, uint8_t *result);
    bool writeRegister(uint8_t reg, uint8_t data);
    
    bool wakeup();
    bool sleep();
    
private:
    
    I2C *_i2c;
    bool _ownI2C;
    
    uint8_t  _address;
    
    int8_t   _temperatureOffset;
    int8_t   _temperatureGain;
    
    int _rawTemperature;
    int _rawField;
    
    Si7210_range_t _range;
    
}; // class Si7210

} // namespace silabs

#endif