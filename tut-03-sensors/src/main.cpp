/***************************************************************************//**
 * @file main.cpp
 *******************************************************************************
 * @section License
 * <b>(C) Copyright 2017 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

#include <string>
#include "mbed.h"
#include "BMP280.h" // barometric pressure sensor
#include "AMS_CCS811.h" // indoor air quality gas sensor
#include "Si7021.h" // relative humidity and temperature sensor
#include "Si7210.h" // hall effect sensor
#include "Si1133.h" // UV index and ambient light sensor
#include "ICM20648.h" // 6-axis inertial sensor

/* Turn on power supply to ENV sensor suite = Si7021, Si1133 and BMP280 */
DigitalOut env_en(PF9, 1);
/* Turn on power to CCS811 sensor */
DigitalOut ccs_en(PF14, 1);
/* Set CCS_WAKE pin to output */
DigitalOut ccs_wake(PF15, 1);
/* Turn on power to hall effect sensor */
DigitalOut hall_en(PB10, 1);
/* Turn on power to IMU */
DigitalOut imu_en(PF8, 1);

I2C ccs_i2c(PB6, PB7);
I2C hall_i2c(PB8, PB9);
I2C env_i2c(PC4, PC5);

void ftos(float num, char *buff);

int main() {
    uint32_t eco2, tvoc;
    float pressure, temperature2;
    int32_t temperature;
    uint32_t humidity;
    float light, uv;
    float acc_x, acc_y, acc_z, gyr_x, gyr_y, gyr_z, temperature3;
    bool lightsensor_en = true;
    bool gassensor_en = true;
    bool hallsensor_en = true;
    bool rhtsensor_en = true;
    bool pressuresensor_en = true;
    bool imu_en = true;
    char buff1[10], buff2[10], buff3[10];

    thread_sleep_for(100);

    printf("\r\n\r\nHello Thunderboard Sense 2...\r\n");

    /* Initialize air quality sensor */
    AMS_CCS811* gasSensor = new AMS_CCS811(&ccs_i2c, PF15);
    if(!gasSensor->init()) {
        printf("Failed CCS811 init\r\n");
        gassensor_en = false;
    } else {
        if(!gasSensor->mode(AMS_CCS811::TEN_SECOND)) {
            printf("Failed to set CCS811 mode\r\n");
            gassensor_en = false;
        }
    }
    
    /* Initialize barometer and RHT */
    BMP280* pressureSensor = new BMP280(env_i2c);
    Si7021* rhtSensor = new Si7021(PC4, PC5);

    /* Initialize Si7210 and check if it is alive */
    silabs::Si7210* hallSensor = new silabs::Si7210(&hall_i2c);
    uint8_t id;
    hallSensor->wakeup();
    hallSensor->readRegister(SI72XX_HREVID, &id);
    printf("Hall ID: %d\r\n", id);

    /* Initialize light sensor */
    Si1133* lightSensor = new Si1133(PC4, PC5);
    if(!lightSensor->open()) {
        printf("Something is wrong with Si1133, disabling...\n");
        lightsensor_en = false;
    }

    /* Initialize the IMU*/
    ICM20648* imu = new ICM20648(PC0, PC1, PC2, PC3, PF12);
    if(!imu->open()) {
        printf("Something is wrong with ICM20648, disabling...\n");
        imu_en = false;
    }
    
    do {
        printf("----------------------------------------------\r\n");
        /* Measure temperature and humidity */
        if(rhtsensor_en) {
            rhtSensor->measure();
            rhtSensor->measure();
            temperature = rhtSensor->get_temperature();
            humidity = rhtSensor->get_humidity();
            printf("  Si7021:\r\n");
            printf("Temperature: %ld.%03ld degC\r\n", temperature/1000, abs(temperature%1000));
            printf("Humidity: %ld.%03ld %%\r\n", humidity/1000, humidity%1000);
        }
        
        /* Measure barometric pressure */
        if(pressuresensor_en) {
            temperature2 = pressureSensor->getTemperature();
            pressure = pressureSensor->getPressure();

            printf("  BMP280:\r\n");
            ftos(pressure, buff1);
            printf("Pressure: %s bar\r\n", buff1);
            ftos(temperature2, buff1);
            printf("Temperature: %s degC\r\n", buff1);
        }

        /* Measure air quality */
        if(gassensor_en) {
            gasSensor->has_new_data();
            eco2 = gasSensor->co2_read();
            tvoc = gasSensor->tvoc_read();

            printf("  CCS811:\r\n");
            printf("CO2: %ld ppm\r\n", eco2);
            printf("VoC: %ld ppb\r\n", tvoc);
        }
        
        /* measure HALL */
        if(hallsensor_en) {
            hallSensor->measureOnce();

            printf("  Si7210:\r\n");
            printf("Temperature: %d.%02d degC\r\n", hallSensor->getTemperature()/1000, abs(hallSensor->getTemperature()%1000));
            printf("Magn. Field Strenth: %i.%03d mT\r\n", (int)(hallSensor->getFieldStrength()/1000), abs(hallSensor->getFieldStrength() % 1000));
        }
        
        /* measure light */
        if(lightsensor_en) {
            lightSensor->get_light_and_uv(&light, &uv);

            printf("  BMP280:\r\n");
            ftos(light, buff1);
            printf("Light: %s lux\r\n", buff1);
            ftos(uv, buff1);
            printf("UV: %s\r\n", buff1);
        }

        /* measure imu */
        if(imu_en) {
            imu->get_temperature(&temperature3);
            imu->get_accelerometer(&acc_x, &acc_y, &acc_z);
            imu->get_gyroscope(&gyr_x, &gyr_y, &gyr_z);

            printf("  ICM20648:\r\n");
            ftos(temperature3, buff1);
            printf("Temperature: %s degC\r\n", buff1);
            ftos(acc_x, buff1);
            ftos(acc_y, buff2);
            ftos(acc_z, buff3);
            printf("Acc: %s %s %s\r\n", buff1, buff2, buff3);
            ftos(gyr_x, buff1);
            ftos(gyr_y, buff2);
            ftos(gyr_z, buff3);
            printf("Gyro: %s %s %s\r\n", buff1, buff2, buff3);
        }

        thread_sleep_for(10000);
    } while(1);
}

void ftos(float num, char *buff){
    if (num<0){
        buff[0]='-';
        buff[1]='\0';
    } else {
        buff[0]='\0';
    }

    int a = (int) abs(num);
    char var[11];
    sprintf(var, "%d", a);
    strcat(buff, var);

    strcat(buff, ".");

    float var1 = (fabs(num)-(float)a)*100; //2 decimal places
    int b = (int) var1;
    if (b < 10){
        strcat(buff, "0");
    }
    sprintf(var, "%d", b);
    strcat(buff, var);
}