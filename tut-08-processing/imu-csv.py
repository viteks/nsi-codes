import serial
from ctypes import Structure, c_uint, c_int, c_float, sizeof
import numpy as np
import csv

connected = False
port = 'COM13'
baud = 115200

class DataPoint(Structure):
    _pack_ = 1
    _fields_ = [
        ("acc_x", c_float), ("acc_y", c_float), ("acc_z", c_float), ("gyr_x", c_float), ("gyr_y", c_float), ("gyr_z", c_float)
    ]

try:
    serial_port = serial.Serial(port, baud)
    serial_port.flushInput()
    connected = True
except:
    print("Unable to open COM port")

def read_from_port(ser):
    #avoid creating local copy of "connected" on assigment
    global connected
    f = open('data.csv', 'w', encoding='UTF8', newline='')
    writer = csv.writer(f)
    H = ['acc_x', 'acc_y', 'acc_z', 'gyr_x', 'gyr_y', 'gyr_z']
    writer.writerow(H)
    it = 0;
    while connected:
        byte = ser.read(sizeof(DataPoint))
        buffer = bytearray(byte)
        data = DataPoint.from_buffer(buffer)
        D = [data.acc_x, data.acc_y, data.acc_z, data.gyr_x, data.gyr_y, data.gyr_z, 2]
        writer.writerow(D)
        it = it + 1
        if it > 10: return

read_from_port (serial_port)