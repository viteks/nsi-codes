import threading
import multiprocessing
import serial
import matplotlib.pyplot as plt
from ctypes import Structure, c_uint, c_int, c_float, sizeof
import numpy as np

connected = False
port = 'COM13'
baud = 115200
BufferLen = 20

class DataPoint(Structure):
    _pack_ = 1
    _fields_ = [
        ("acc_x", c_float),
        ("acc_y", c_float),
        ("acc_z", c_float),
        ("gyr_x", c_float),
        ("gyr_y", c_float),
        ("gyr_z", c_float)
    ]

try:
    serial_port = serial.Serial(port, baud)
    serial_port.flushInput()
    connected = True
except:
    print("Unable to open COM port")

def update(q):
    #avoid creating local copy of "connected" on assigment
    global connected
    plt.ion()
    fig, (ax1, ax2) = plt.subplots(2)
    y1 = np.array(np.zeros([200]))
    y2 = np.array(np.zeros([200]))
    y3 = np.array(np.zeros([200]))
    y4 = np.array(np.zeros([200]))
    y5 = np.array(np.zeros([200]))
    y6 = np.array(np.zeros([200]))
    line1, = ax1.plot(y1)
    line2, = ax1.plot(y2)
    line3, = ax1.plot(y3)
    line4, = ax2.plot(y4)
    line5, = ax2.plot(y5)
    line6, = ax2.plot(y6)

    while connected:
        data = 0.
        DataBuff = np.zeros([6, BufferLen])
        try:
            # data = q.get_nowait()
            for i in range(BufferLen):
                data = q.get()
                DataBuff[0][i] = data.acc_x
                DataBuff[1][i] = data.acc_y
                DataBuff[2][i] = data.acc_z
                DataBuff[3][i] = data.gyr_x
                DataBuff[4][i] = data.gyr_y
                DataBuff[5][i] = data.gyr_z
            # print("data: {} {}".format(data.acc_x, data.acc_y))
        except Exception as e:
            print(".", e)

        Lag = q.qsize()
        if Lag > 0:
            print("LAG: ", Lag)

        y1 = np.append(y1, DataBuff[0])
        y1 = y1[BufferLen:201+BufferLen]
        line1.set_ydata(y1)
        #
        y2 = np.append(y2, DataBuff[1])
        y2 = y2[BufferLen:201+BufferLen]
        line2.set_ydata(y2)
        #
        y3 = np.append(y3, DataBuff[2])
        y3 = y3[BufferLen:201+BufferLen]
        line3.set_ydata(y3)
        # 
        y4 = np.append(y4, DataBuff[3])
        y4 = y4[BufferLen:201+BufferLen]
        line4.set_ydata(y4)
        #
        y5 = np.append(y5, DataBuff[4])
        y5 = y5[BufferLen:201+BufferLen]
        line5.set_ydata(y5)
        #
        y6 = np.append(y6, DataBuff[5])
        y6 = y6[BufferLen:201+BufferLen]
        line6.set_ydata(y6)
        #

        ax1.set_ylim(np.min([y1, y2, y3]), np.max([y1, y2, y3]))
        ax2.set_ylim(np.min([y3, y4, y5]), np.max([y3, y4, y5]))
        #error => window is closed, signal end to other threads
        try:
            fig.canvas.draw()
            fig.canvas.flush_events()
        except Exception as e:
            print(e)
            connected = False

def read_from_port(q, ser):
    #avoid creating local copy of "connected" on assigment
    global connected
    while connected:
        byte = ser.read(sizeof(DataPoint))
        buffer = bytearray(byte)
        Point = DataPoint.from_buffer(buffer)
        q.put(Point)

if __name__ == '__main__':
    q = multiprocessing.Queue()
    t = threading.Thread(target=read_from_port, args=(q, serial_port,))
    t.start()

    update(q)

    t.join()

    #clear remaining items to allow queue to end worker threads
    #https://stackoverflow.com/questions/31170788/python-multiprocessing-threads-dont-close-when-using-queue
    while not q.empty(): 
        q.get()
