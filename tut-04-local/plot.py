import serial
import matplotlib.pyplot as plt
import numpy as np

plot_window = 20
y0_var = np.array(np.zeros([plot_window]))
y1_var = np.array(np.zeros([plot_window]))

plt.ion()
fig, ax = plt.subplots(2)
line0, = ax[0].plot(y0_var)
line1, = ax[1].plot(y1_var)
ax[0].set_ylim([0, 255])
ax[1].set_ylim([0, 255])

try:
    ser = serial.Serial('COM13', 115200)
    ser.flushInput()
except:
    print("unable to open COM port")

while True:
    byte = ser.read(2)
    decoded_bytes = int.from_bytes(byte, byteorder = 'little')
    a = decoded_bytes & 255;
    b = (decoded_bytes >> 8) & 255;
    # print(decoded_bytes)
    y0_var = np.append(y0_var, a)
    y1_var = np.append(y1_var, b)
    y0_var = y0_var[1:plot_window+1]
    y1_var = y1_var[1:plot_window+1]
    line0.set_ydata(y0_var)
    line1.set_ydata(y1_var)
    fig.canvas.draw()
    fig.canvas.flush_events()