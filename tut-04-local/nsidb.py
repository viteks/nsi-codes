import sqlite3
from sqlite3 import Error

def db_conn(file):
    """ Create a database connection
    : param file: name of database file
    : return: 
    """
    conn = None
    try:
        conn = sqlite3.connect(file)
    except Error as e:
        print("Error", e)
    
    return conn

def db_exec(conn, sql, data = None):
    """ execute a SQL command
    :param conn: Connection object
    :param sql:  SQL statement
    """
    try:
        c = conn.cursor()
        if data is not None:
            c.execute(sql, data)
        else:
            c.execute(sql)
    except Error as e:
        print(e)
    
    return c

def db_create(conn):

    sql = """CREATE TABLE IF NOT EXISTS Si7021 (id integer PRIMARY KEY, timestamp integer NOT NULL, temperature integer NOT NULL, humidity integer NOT NULL);"""
    cur = db_exec(conn, sql)

def db_insert(conn, data):
    """ Create a new record
    :param conn:    Connection object
    :param data:    tuple of data
    :return:        ID of last inserted row
    """

    sql = """ insert into Si7021 (timestamp, temperature, humidity) values (datetime(), ?, ?); """
    cur = db_exec(conn, sql, data)
    conn.commit()

    return cur.lastrowid

def db_fetch(conn):
    """
    Fetch data from table
    :param conn: the Connection object
    :return:
    """
    cur = db_exec(conn, "SELECT * FROM Si7021")

    return cur.fetchall()
