import serial
import paho.mqtt.subscribe as subscribe

try:
    serial_port = serial.Serial('COM13', 115200)
    serial_port.flushInput()
except:
    print("unable to open COM port")

def print_msg(client, userdata, message):
    # print("%s : %s" % (message.topic, message.payload))
    data = message.payload.decode('ascii')
    print(data)
    if data == "red":
        print("switching red")
        serial_port.write(b'r')
    if data == "green":
        print("switching green")
        serial_port.write(b'g')


subscribe.callback(print_msg, "nsi/standa", hostname="mqtt.eclipseprojects.io")
