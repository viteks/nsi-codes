from PyQt6.QtCore import * 
from PyQt6.QtGui import * 
from PyQt6.QtWidgets import *

import paho.mqtt.publish as publish
import paho.mqtt.subscribe as subscribe
import serial

def red():
    print("hello")
    publish.single("nsi/standa", "red", hostname="mqtt.eclipseprojects.io")

def green():
    print("hello")
    publish.single("nsi/standa", "green", hostname="mqtt.eclipseprojects.io")


if __name__ == '__main__':
    app = QApplication([])
    
    dlg = QDialog()

    layout = QVBoxLayout()
    # button1
    button1 = QPushButton("red")
    button1.clicked.connect(red)
    layout.addWidget(button1)
    # button2 
    button2 = QPushButton("green")
    button2.clicked.connect(green)
    layout.addWidget(button2)
    # 
    dlg.setLayout(layout)

    dlg.show()
    app.exec()
