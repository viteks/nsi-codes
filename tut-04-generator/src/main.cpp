#include <mbed.h>

BufferedSerial pc(USBTX, USBRX, 115200);

class T
{
	Ticker t;
	volatile int count;
	unsigned char send[2];

	void cblk()
	{
		count += 10;
		send[0] = 127 + (127 * sin(1 * 6.28 * (count / 180.0)));
		send[1] = 255 - send[0];
		pc.write(&send, 2);
	}

public:
	T(float cas):count(0)
	{
		t.attach (callback (this, &T::cblk), cas);
	}	
};

int main() 
{
	T x(.05);

	while(1) {}
}
