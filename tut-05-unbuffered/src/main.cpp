#include <mbed.h>

UnbufferedSerial serial_port(USBTX, USBRX, 115200);
DigitalOut R_LED(LED0); // LED0 = PD8 ~ RED, LED1 = PD9 ~ GREEN
DigitalOut G_LED(LED1);

void serial_isr()
{
    char c;
    if (serial_port.read(&c, 1))
    {                             // Read the data to clear the receive interrupt.
        serial_port.write(&c, 1); // Echo the input back to the terminal.
        if (c == 'r')
            R_LED = !R_LED;
        if (c == 'g')
            G_LED = !G_LED;
    }
}

int main()
{
    serial_port.attach(&serial_isr, SerialBase::RxIrq);
    while (1)
    {
    }
}