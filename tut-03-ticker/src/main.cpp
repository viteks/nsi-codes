#include <mbed.h>

#define R_LED_TOGGLE_TIME 0.5 // in seconds
#define G_LED_TOGGLE_TIME 1.0 // in seconds

DigitalOut R_LED(LED0); // LED0 = PD8 ~ RED
DigitalOut G_LED(LED1); // LED1 = PD9 ~ GREEN

BufferedSerial serial (USBTX, USBRX, 115200);

Ticker flipper1;
Ticker flipper2;

void flipper1_isr(){
  R_LED = !R_LED;
}
void flipper2_isr(){
  G_LED = !G_LED;
}

int main() {
  flipper1.attach(flipper1_isr, R_LED_TOGGLE_TIME);
  flipper2.attach(flipper2_isr, G_LED_TOGGLE_TIME);

  while(1) {
    serial.write("ahoj\n\r", 5);
  }
}